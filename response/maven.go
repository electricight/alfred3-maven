package response

type Maven struct {
	ArtifactId       string `json:"artifactId"`
	LatestVersion string `json:"latestVersion"`
	GroupId           string `json:"groupId"`
}

func (m *Maven) SetProperties(ArtifactId,GroupId ,LatestVersion string) {
	m.ArtifactId = ArtifactId
	m.LatestVersion = LatestVersion
	m.GroupId=GroupId
}
