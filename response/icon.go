package response

type Icon struct {
	IType string `json:"type"`
	Path  string `json:"path"`
}
