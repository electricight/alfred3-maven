package response

type Info struct {
	Uid          string `json:"uid"`
	IType        string `json:"type"`
	Title        string `json:"title"`
	Subtitle     string `json:"subtitle"`
	Arg          string `json:"arg"`
	Autocomplete string `json:"autocomplete"`
	Icon         Icon   `json:"icon"`
}

func(i *Info) SetProperties(uid,itype,title,subtitle,arg,autocomplete string,icon Icon){
	i.Uid=uid
	i.IType=itype
	i.Title=title
	i.Subtitle=subtitle
	i.Arg=arg
	i.Autocomplete=autocomplete
	i.Icon=icon
}
